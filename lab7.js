// task 1

let animal = {
    jumps: null
};
let rabbit = {
    __proto__: animal,
    jumps: true
};

console.log(rabbit.jumps); // ? (1) true

delete rabbit.jumps;

console.log(rabbit.jumps); // ? (2) null

delete animal.jumps;

console.log(rabbit.jumps); // ? (3) undefined

// task 2

let head = {
    glasses: 1
};

let table = {
    pen: 3,
    __proto__: head
};

let bed = {
    sheet: 1,
    pillow: 2,
    __proto__: table
};

let pockets = {
    money: 2000,
    __proto__: bed
};

console.log(pockets.pen) // Output 3
console.log(bed.glasses) // Output 1

// task 3

let hamster = {
    stomach: [],

    eat(food) {
        this.stomach.push(food);
    }
};

let speedy = {
    stomach: [],
    __proto__: hamster
};

let lazy = {
    stomach: [],
    __proto__: hamster
};

// Цей хом’ячок знайшов їду
speedy.eat("apple");
console.log(speedy.stomach); // apple

// Але цей також має їжу, чому? Виправте це.
console.log(lazy.stomach); // apple

// task 4

let dictionary = Object.create(null, {
    toString: {
        value: function () {
            return "ToString function"
        }
    }
});

// ваш код, щоб додати dictionary.toString метод

// додаємо певні дані
dictionary.apple = "Яблуко";
dictionary.__proto__ = "тест"; // __proto__ тут є звичайною властивістю об’єкта

// тільки ключі apple та __proto__ показуються в циклі
for (let key in dictionary) {
    console.log(key); // "apple", потім "__proto__"
}

// ваш метод toString в дії
console.log(dictionary.toString()); // "apple,__proto__"

// task 5

Object.__proto__.defer = function (ms) {
    let currentFunc = this;
    return function (a, b) {
        setTimeout(() => currentFunc(a, b), ms);
    }
}

function f(a, b) {
    console.log(a + b);
}

f.defer(1000)(5, 10);



